module.exports = grunt => {
    const fs = require('fs');

    /**
     * List of files to process
     *
     * Format:
     *   'DESTINATION PATH': [ 'LIST', 'OF', 'PATHS', 'TO', 'FILES' ]
     */
    const CssFileList = {
        'public/css/site.min.css': 'public/css/site.scss'
    };

    let gruntConfig = {
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: CssFileList
            }
        },
        postcss: {
            options: {
                map: true,
                processors: [
                    require('postcss-font-magician')({}),
                    require('pixrem')(),
                    require('autoprefixer')({ browsers: '> 0.01%' }),
                    require('cssnano')()
                ]
            },
            dist: {
                files: {}
            }
        },
        closurecompiler: {},
        javascript_obfuscator: {
            options: {
                debugProtection: true,
                debugProtectionInterval: true,
                domainLock: [ '.landofnevard.net' ]
            },
            main: {
                files: {}
            }
        },
        watch: {
            scripts: {
                files: [ 'public/js/*.js', '!public/js/*.min.js' ],
                tasks: [ 'closurecompiler', 'cleanup' ],
                options: {
                    spawn: true
                }
            },
            styles: {
                files: [ 'public/css/**/*.scss', '!public/css/**/*.min.css' ],
                tasks: [ 'sass', 'postcss' ],
                options: {
                    spawn: true
                }
            }
        }
    };

    Object.keys(CssFileList).forEach(final => {
        gruntConfig.postcss.dist.files[ final ] = final;
    });

    grunt.initConfig(gruntConfig);

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');

    grunt.registerTask('default', [ 'sass', 'postcss' ]);
};
