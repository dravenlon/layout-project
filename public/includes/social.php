<div id="social-box" class="nav social">
    <div id="social-header">-Social-</div>
    <div class="nav-1 align-center">
        <a href="http://www.facebook.com/landofnevard" id="facebook_button" title="<?= SITE_NAME ?> on Facebook" target="_blank">
            <img src="/images/facebook.png" alt="<?= SITE_NAME ?> on Facebook" width="50px" height="50px">
        </a>
        <a href="https://twitter.com/landofnevard" id="twitter_button" title="<?= SITE_NAME ?> on Twitter" target="_blank">
            <img src="/images/twitter.png" alt="<?= SITE_NAME ?> on Twitter" width="50px" height="50px">
        </a>
    </div>
</div>
