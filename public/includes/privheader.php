<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="content-language" content="english">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Michael Delle">
        <meta name="copyright" content="Copyright © <?= date('Y') ?> Michael Delle">
        <meta name="page-topic" content="<?= $title ?>">

        <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">

        <title>Land of Nevard - <?= $title ?></title>

        <link rel="stylesheet" href="/css/tooltipster.bundle.min.css?v=<?= GAME_VERSION ?>">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/ui-darkness/jquery-ui.min.css">
        <link rel="stylesheet" href="/css/site.min.css?v=<?= GAME_VERSION ?>">

        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <![endif]-->

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"
                integrity="sha256-JklDYODbg0X+8sPiKkcFURb5z7RvlNMIaE3RA2z97vw=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.6/vue.min.js"
                integrity="sha256-cWZZjnj99rynB+b8FaNGUivxc1kJSRa8ZM/E77cDq0I=" crossorigin="anonymous"></script>
        <script src="/js/site.min.js?v=<?= GAME_VERSION ?>"></script>
    </head>
    <body>
        <div id="distance"></div>
        <div id="wrapper" class="">

            <?php require INCLUDES . 'masthead.php' ?>

            <?php require INCLUDES . 'subnavbar.php' ?>

            <?php require INCLUDES . 'stats.php' ?>
            <?php require INCLUDES . 'navbar.php' ?>
            <?php require INCLUDES . 'help.php' ?>
            <?php require INCLUDES . 'social.php' ?>
            <?php require INCLUDES . 'donate.php' ?>

            <main id="main" class="main<?= ((isset($page_class) && !empty($page_class)) ? " $page_class" : '') ?>" role="main">
                <div class="main-2" id="outwindow">
