</div>
</main>
<footer id="info" class="footer" role="contentinfo">
    <ul class="info">
        <li class="list-info-copyright">
            &copy; 2004-<?= date("Y") ?> <?= SITE_NAME ?>
        </li>
        <li class="list-info-game-version">
            Game v<?= GAME_VERSION ?>
        </li>
        <li class="list-info-developed">
            Developed by <a href="http://www.michaeldelle.com" target="_blank">Michael Delle</a>
        </li>
        <li class="list-info-nav nav">
            <ul class="nav-1">
                <li class="list-nav-1">
                    <a href="/terms.php" title="Terms of Service" target="_blank">Terms of Service</a>
                </li>
                <li class="list-nav-1">
                    <a href="http://www.michaeldelle.com/contact/" title="Contact Michael Delle" target="_blank">Contact</a>
                </li>
            </ul>
        </li>
    </ul>
</footer>
</div>
</div>
</div>
<?php require_once INCLUDES . 'chat.php'; ?>
</body>
</html>
