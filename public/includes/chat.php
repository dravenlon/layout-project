<noscript id="chat-nojs">
    <style>
        #chat {
            display : none;
        }
    </style>
    <div id="nojs">
        JavaScript is required to use the chat
    </div>
</noscript>
<chat id="chat" :messages="messages" :notifications="notifications" :armouryRequests="armouryRequests" :clanInvites="clanInvites" :clanApplications="clanApplications" :events="events"></chat>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js" integrity="sha256-Ikk5myJowmDQaYVCUD0Wr+vIDkN8hGI58SGWdE671A8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js" integrity="sha256-lDaoGuONbVHFEV6ZW3GowBu4ECOTjDE14hleNVBvDW8=" crossorigin="anonymous"></script>
<!--[if IE 8]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-linkify/2.1.4/linkify-polyfill.min.js" integrity="sha256-wmpnkoJHO0yfeVBTdUdz/cgNTnb79WlZg5YsOnqEHkU="crossorigin="anonymous"></script>
<![endif]-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-linkify/2.1.4/linkify.min.js" integrity="sha256-/qh8j6L0/OTx+7iY8BAeLirxCDBsu3P15Ci5bo7BJaU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-linkify/2.1.4/linkify-html.min.js" integrity="sha256-Isk4oQgoXMJM3p6Y5TvbWckheA1zatXexdsEGSslq/Q=" crossorigin="anonymous"></script>
