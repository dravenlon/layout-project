<nav id="navbar" class="nav mainnav">
    <div id="mainnav-header">-Navigation-</div>
    <ol class="nav-1">
        <li class="list-nav-1"><a href="/main.php" title="Your Cabin">Your Cabin</a></li>
        <li class="list-nav-1 link-pack-mule"><a href="/pmule.php">Pack Mule</a></li>
        <li class="list-nav-1 link-healing-falls"><a href="/healingfalls.php">Healing Falls</a></li>
        <li class="hr"></li>
        <li class="list-nav-1"><a href="/battle.php">Battle</a></li>
        <li class="list-nav-1 link-auto-fighter"><a href="/_autofighter.php">Auto Fighter</a></li>
        <li class="list-nav-1 link-wilderness"><a href="/wilderness.php" target="refresher">Wilderness</a></li>
        <li class="hr"></li>
        <li class="list-nav-1 link-necropolis"><a href="/necropolis.php">Necropolis</a></li>
        <li class="list-nav-1 link-necropolis-treasury"><a href="/treasury.php">Necropolis Treasury</a></li>
        <li class="list-nav-1"><a href="/bank.php">Maenmah Bank</a></li>
        <li class="hr"></li>
        <li class="list-nav-1"><a href="/forum.php">Forum</a></li>
        <li class="list-nav-1"><a href="/postoffice.php">Post Office</a></li>
        <li class="hr"></li>
        <li class="list-nav-1"><a href="/members.php?limit=0">Members</a></li>
        <li class="list-nav-1"><a href="/ponline.php">Players Online (<span id="statponline">20</span>)</a></li>
        <li class="list-nav-1"><a href="/game_updates.php">Game Updates</a></li>
        <li class="list-nav-1"><a href="/account.php">Account</a></li>
        <li class="list-nav-1"><a href="/login.php?user=logout" class="link-logout" title="Logout">Logout</a></li>
    </ol>
</nav>
