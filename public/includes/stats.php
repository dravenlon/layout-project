<div id="stats-box" class="nav stats">
    <div id="stats-header">-Stats-</div>
    <ol class="stats-1">
        <li><b>Name:</b> <span class="statname">Draven</span></li>
        <li><b>ID:</b> <span class="statid">1</span></li>
        <li><b>Level:</b> <span id="statlevel">1</span></li>
        <li><b>Life:</b> <span id="statlife">15/15</span></li>
        <li><b>Exp:</b> <span id="statexp">0/100</span></li>
        <li><b>Hand:</b> <span id="stathand">500</span></li>
        <li><b>Bank:</b> <span id="statbank">1500</span></li>
        <li><b>Fights:</b> <span class="statfights">0/50</span></li>
        <li><b>Battle Points:</b> <span id="statbp">8</span></li>
        <li id="time">
            <span id="time-days"><?= date('M d, Y') ?></span>
            <span id="time-hours"><?= date('g:i:s A') ?></span>
        </li>
    </ol>
</div>
