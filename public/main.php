<?php

$title = "Your Cabin";
$page_class = 'homepage';

include 'includes/privheader.php';
?>

    <div id="main-character">
        <div class="avatar-box">'
            <img src="images/profile/boybarb.gif" border="0">
        </div>
        <div id="player-motto">Player Motto</div>
    </div>

    <div class="subcontent">
        <h2>Stats</h2>
        <ul>
            <li><b>Class:</b> Warrior</li>
            <li><b>Element:</b> Lightning</li>
            <li><b>Level:</b> 1</li>
            <li><b>Experience:</b> 0/100</li>
            <li><b>Life:</b> 15/15</li>
            <li><b>Days Old:</b> 1</li>
            <li><b>Total Logins:</b> 1</li>
            <li><b>Last Login:</b> 2018-03-25 21:24:38</li>
            <li><b>Donation Points:</b> 8</li>
            <li><b>Donated To Game:</b> 0.00</li>
        </ul>
    </div>

    <div class="subcontent">
        <h2 class="h-stats">Other Items</h2>
        <ul>
            <li><b>Sapphires:</b> 100</li>
            <li><b>Amethysts:</b> 100</li>
            <li><b>Garnets:</b> 100</li>
            <li><b>Keys:</b> 100</li>
            <li><b>F.Stones:</b> 100</li>
        </ul>
    </div>

<?php
include 'includes/privfooter.php';
